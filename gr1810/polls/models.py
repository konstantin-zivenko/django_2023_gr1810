import datetime
from zoneinfo import ZoneInfo

from django.db import models


class Question(models.Model):
    question_text = models.CharField(max_length=200, null=False)
    pub_date = models.DateTimeField("date published")

    def __str__(self):
        return f"Question: id: {self.id}, text: {self.question_text}"

    def was_published_recently(self):
        return self.pub_date >= datetime.datetime.now(tz=ZoneInfo("UTC")) - datetime.timedelta(days=1)


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return f"Choice: id: {self.id}, text: {self.choice_text}"

