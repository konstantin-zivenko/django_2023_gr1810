from django.urls import path

from . import views

urlpatterns = [
    path("", views.citizen_kane, name="index"),
    path("casablanca/", views.casablanca),
    path("maltese_falcon/", views.maltese_falcon),
    path("psycho/", views.psycho),
    path("listing/", views.listing),
]