# django_2023_gr1810

---

[video 1](https://youtu.be/ps-obMDTArA)

[video 2 part 1](https://youtu.be/AcxMcgs_YBQ)

[video 2 part 2](https://youtu.be/ULSDJYkQ71I)

[video 3](https://youtu.be/NMhXMqiS2o0) - models and admin

[vide0 4](https://youtu.be/n6OrEiPc86c) - views and templates

[video 5](https://youtu.be/Fm6hnvBZDNk) - templates (tags, filter)

[video 6](https://youtu.be/41bL9OLYLjg) - views as class, forms

[video 7](https://youtu.be/T3GznzlRUns) - forms, DRF

[video 8](https://youtu.be/ptiOYAXnYaQ) - auth, part 1

[video 9](https://youtu.be/uCdOEH0FxdE) - auth, part 2


