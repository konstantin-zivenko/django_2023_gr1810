from django.urls import path
from user_management.views import dashboard, register

urlpatterns = [
    path("dashboard/", dashboard, name="dashboard"),
    path("register/", register, name="register")
]