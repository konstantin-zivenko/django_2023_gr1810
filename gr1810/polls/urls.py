from django.urls import path

from . import views

app_name = "polls"
urlpatterns = [
    path('', views.index, name="index"),
    path("<int:question_id>/results/", views.results, name="results"),
    path("<int:question_id>/", views.detail, name="detail"),
    path("<int:question_id>/vote/", views.vote, name="vote"),
    path("user/", views.user_name, name="user_name"),
    path("<str:user_name>/thanks/", views.thanks, name="thanks"),
    path("api/v1/questions/", views.question_collection),
    path("api/v1/questions/<int:pk>/", views.question_element)
]