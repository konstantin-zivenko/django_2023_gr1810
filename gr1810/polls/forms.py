from django import forms

class NameForm(forms.Form):
    your_name = forms.CharField(label="Your name", max_length=100)
    birthday = forms.DateField(label="birthday: ", widget=forms.SelectDateWidget)
    email = forms.EmailField(label="your email: ")

