from django.http import HttpResponse, HttpRequest, HttpResponseRedirect
from django.template import loader
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from .forms import NameForm
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import QuestionSerializer
from .models import Question, Choice


def index(request: HttpRequest) -> HttpResponse:
    latest_questions = Question.objects.order_by("-pub_date")[:5]
    context = {
        "latest_questions": latest_questions
    }
    return render(request, "polls/index.html", context)


def detail(request: HttpRequest, question_id: int) -> HttpResponse:
    question = get_object_or_404(Question, pk=question_id)
    return render(request, "polls/detail.html", {"question": question})


def results(request: HttpRequest, question_id: int) -> HttpResponse:
    question = get_object_or_404(Question, pk=question_id)
    return render(request, "polls/results.html", {"question": question})


def vote(request: HttpRequest, question_id: int) -> HttpResponse:
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST["choice"])
    except (KeyError, Choice.DoesNotExist):
        return render(request, "polls/detail.html", {
            "question": question,
            "error_message": "You didn`t select a choice."
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        return HttpResponseRedirect(reverse("polls:results", args=(question_id,))) # /polls/3/results/


def user_name(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form = NameForm(request.POST)
        if form.is_valid():
            your_name = form.cleaned_data["your_name"]
            return HttpResponseRedirect(f"/polls/{your_name}/thanks/")
    else:
        form = NameForm()
    return render(request, "polls/user_name.html", {"form": form})


def thanks(request: HttpRequest, user_name: str) -> HttpResponse:
    return render(request, "polls/thanks.html", {"user_name": user_name})


@api_view(['GET'])
def question_collection(request):
    if request.method == 'GET':
        questions = Question.objects.all()
        serializer = QuestionSerializer(questions, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def question_element(request, pk):
    try:
        question = Question.objects.get(pk=pk)
    except:
        return HttpResponse(status=404)
    
    if request.method == 'GET':
        serializer = QuestionSerializer(question)
        return Response(serializer.data)