from django.urls import path

from . import views_

app_name = "polls"
urlpatterns = [
    path('', views_.IndexView.as_view(), name="index"),
    path("<int:pk>/results/", views_.ResultsView.as_view(), name="results"),
    path("<int:pk>/", views_.DetailView.as_view(), name="detail"),
    path("<int:question_id>/vote/", views_.vote, name="vote"),
]