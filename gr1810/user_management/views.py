from django.shortcuts import render, redirect
from django.contrib.auth import login
from django.urls import reverse
from user_management.forms import CustomerUserCreationForm



def dashboard(request):
    return render(request, "user_management/dashboard.html")


def register(request):
    if request.method == "GET":
        return render(
            request,
            "user_management/register.html",
            {"form": CustomerUserCreationForm()}
        )
    elif request.method == "POST":
        form = CustomerUserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect(reverse("dashboard"))

